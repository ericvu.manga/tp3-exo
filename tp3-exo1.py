# 1. Ecrire un script tp3-exo1.py qui produira un dossier flagsBis
# Le dossier flagsBis contiendra tous les fichiers png situés dans le dossier flags mais
# renommés en ne conservant que les deux premières lettres du nom d'origine.
# Ces lettres seront en majuscule.
# ex: 
#   flags/allemagne.png => flagsBis/AL.png
#   flags/belgique.png => flagsBis/BE.png
# Le fichier missing.png devra être ignoré
# En option: mettre en place une gestion d'erreur
import os
import shutil

if not os.path.exists("flagBis"):
    os.mkdir("flagBis")
    print("[+] flagBis dir created")
else:
    print("flagBis dossier existe deja")

if len(os.listdir("flagBis")) == 0:
    for f in os.listdir("flags"):
        if not f.startswith("missing"):
            #filepath = "files/flagBis" + f[:2].upper() + ".png"
            shutil.copy("flags/"+ f, "flagBis/"+ f[:2].upper() + ".png")
            print("[+] file %s copied" % f)
else:
    print("les images existent deja")
