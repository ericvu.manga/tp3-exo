# 2. Ecrire un script tp3-exo2.py qui, à partir du fichier deniro.csv,
# produira en sortie un fichier deniro-report.txt" 
# Ce fichier affichera les informations suivantes:
# Nom du film le mieux noté
# Nombre de films entre 2000 et 2010
# En option: mettre en place une gestion d'erreur
# Astuce: il existe un module natif "csv" (voir sa documentation)

import os
import csv
#import pandas as pd


f = open("test.csv","r")
content = csv.reader(f)

header = []
header = next(content)
#print(header)

#recupere le contenu
rows = []
for row in content:
        rows.append(row)

def maximum(liste):
    return max(liste)

#parcours la colonne des Score et recupere le max score
# i = 0   
# score = []
# while i < len(rows):

#         score.append(rows[i][1])
#         i += 1

# max = maximum(score)

# a = [x for x in rows if x[1] == max print("ok" + x[2])]
# print(a)
maxi = 0
top = ""
# affiche le film avec le plus grand score
for note in rows:
        if int(note[1]) >= maxi:
                maxi = int(note[1]) 
                top = note[2]
        # if note[1] == max:

print("Nom du film le mieux noté = " + top)

# compte les nombres de film entre 2000 et 2010
count = 0
for année in rows:
        if 2000 <= int(année[0]) <= 2010:
                count += 1

print("Nombre de films entre 2000 et 2010 = %d" %(count))

filepath = "deniro-report.txt"
f2 = open(filepath,"w")
f2.write("Nom du film le mieux noté = " + note[2]+"\n")
f2.write("Nombre de films entre 2000 et 2010 = %d" %(count))

f.close()
f2.close()